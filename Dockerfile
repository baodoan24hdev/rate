FROM golang:1.14.6-alpine3.12 as builder
COPY go.mod go.sum /go/src/gitlab.com/baodoan24hdev/rate/
WORKDIR /go/src/gitlab.com/baodoan24hdev/rate
RUN go mod download
COPY . /go/src/gitlab.com/baodoan24hdev/rate
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/rate gitlab.com/baodoan24hdev/rate

FROM alpine
RUN apk add --no-cache ca-certificates && update-ca-certificates
COPY --from=builder /go/src/gitlab.com/baodoan24hdev/rate/build/rate /usr/bin/rate
EXPOSE 8081 8081
ENTRYPOINT ["/usr/bin/rate"]