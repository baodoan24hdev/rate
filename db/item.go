package db

import (
	"database/sql"
	"strings"

	"gitlab.com/baodoan24hdev/rate/models"
)

func (db Database) GetAllItems() (*models.ItemList, error) {
	list := &models.ItemList{}
	rows, err := db.Conn.Query("SELECT * FROM rates ORDER BY ID DESC")
	if err != nil {
		return list, err
	}
	for rows.Next() {
		var item models.Item
		err := rows.Scan(&item.ID, &item.Time, &item.Currency, &item.Rate, &item.CreatedAt)
		if err != nil {
			return list, err
		}
		list.Items = append(list.Items, item)
	}
	return list, nil
}

func (db Database) AddAllItem(data *models.Envelope) error {
	var stmt *sql.Stmt
	query := "INSERT INTO rates (time, currency, rate) VALUES "
	vals := []interface{}{}

	for _, cubes := range data.Envelope.BigCube {
		time := cubes.Time
		for _, cube := range cubes.Cubes {
			query += "(1, AUD, 2),"
			vals = append(vals, time, cube.Currency, cube.Rate)
		}
	}

	query = strings.TrimSuffix(query, ",")
	stmt, _ = db.Conn.Prepare(query)
	stmt.Exec(vals...)

	return nil
}
