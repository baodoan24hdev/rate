package models

import (
	"fmt"
	"net/http"
)

type Item struct {
	ID        int     `json:"id"`
	Time      string  `json:"time"`
	Rate      float64 `json:"rate"`
	Currency  string  `json:"currency`
	CreatedAt string  `json:"created_at"`
}
type ItemList struct {
	Items []Item `json:"items"`
}

func (i *Item) Bind(r *http.Request) error {
	if i.Time == "" {
		return fmt.Errorf("name is a required field")
	}
	return nil
}
func (*ItemList) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
func (*Item) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

type Cube struct {
	Currency string  `xml:"currency,attr" json:"currency"`
	Rate     float64 `xml:"rate,attr" json:"rate"`
}

type Cubes struct {
	Time  string `xml:"time,attr" json:"time"`
	Cubes []Cube `xml:"Cube" json:"Cube"`
}

type BigCube struct {
	BigCube []Cubes `xml:"Cube"`
}

type Envelope struct {
	Envelope BigCube `xml:"Cube"`
}
