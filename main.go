package main

import (
	"encoding/xml"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gitlab.com/baodoan24hdev/rate/db"
	"gitlab.com/baodoan24hdev/rate/models"
)

var dbInstance db.Database

func main() {
	db.Initialize("rate", "rate_pass", "rate_db")
	resp, err := http.Get("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml")
	data := &models.Envelope{}
	if err != nil {
		log.Fatalln(err)
	}
	dbUser, dbPassword, dbName :=
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_DB")

	database, _ := db.Initialize(dbUser, dbPassword, dbName)

	if err != nil {
		log.Fatalf("Could not set up database: %v", err)
	}
	defer database.Conn.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		log.Fatalln(err)
	}

	xml.Unmarshal(body, &data)

	dbInstance.AddAllItem(data)
}
